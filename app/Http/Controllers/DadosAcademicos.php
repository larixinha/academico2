<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\aluno;
use App\Models\nota;
use App\Models\falta;




class DadosAcademicos extends Controller
{
public function ListaAlunos(){
    $alunos = aluno::all() ;

    return  view('pagina2')->with(compact('alunos'));
}
public function ListaDados($id){
    $nome  = aluno::where(['id' => $id])->select('nome')->first()->nome;
    $notas = nota::where(['id_aluno' => $id])->get();
    $faltas = falta::where(['id_aluno' => $id])->get();

    return view('pagina3')->with(compact('nome','notas','faltas'));
}
public function cadastraAlunos(){
    return view('cadastraAlunos');
}

}
       

